variable "region" {
  type    = string
  default = "eu-west-2"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "project" {
  type    = string
  default = "PMI"
}

variable "dept_name" {
  type    = string
  default = "sales"
}

variable "app_name" {
  type    = string
  default = ".Net Core on AWS"
}

variable "vpc_cidr_range" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnets" {
  type    = list(string)
  default = ["10.0.0.0/24", "10.0.1.0/24"]
}

variable "private_subnets" {
  type = list(string)
  default = ["10.0.2.0/24", "10.0.3.0/24"]
}


variable "default_tags" { 
    type = "map" 
    default = { 
        env: "${var.env}",
        project: "${var.project}",
        department: "${var.dept_name}",
        app: "${var.app_name}"
  } 
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "vpc_tags" {
  description = "Additional tags for the VPC"
  type        = map(string)
  default     = {}
}