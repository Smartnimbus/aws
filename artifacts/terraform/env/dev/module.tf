# DATA SOURCES
data "aws_availability_zones" "azs" {}


module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.33.0"

  name = "${var.env}-vpc"
  cidr = var.vpc_cidr_range

  azs            = slice(data.aws_availability_zones.azs.names, 0, 2)
  public_subnets = var.public_subnets

  # Private subnets
  private_subnets  = var.private_subnets
  private_subnet_group_tags = {
    subnet_type = "private"
  }

  enable_nat_gateway = true
  #enable_vpn_gateway = true

  tags = var.default_tags

}
